package biblioteca.repository.repo;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepositoryMock;
import biblioteca.util.Validator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class CartiRepositoryTest {

    BibliotecaCtrl controller;
    CartiRepository cartiRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception{
        this.cartiRepository = new CartiRepository();
        this.controller = new BibliotecaCtrl(cartiRepository);
    }

    @Test
    public void TC1_ECP() {
        //date valide
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("Titlu");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Ed");
        try {
            controller.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC2_ECP() throws Exception{
        //titlu nu e string
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Titlu invalid!");
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("22");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Edit");
      //  try {
              controller.adaugaCarte(carte);
     //   }catch (Exception ex){
       //     System.out.println(ex.toString());
      //  }

        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
       // assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC3_BVA() throws Exception{
        //titlu = ""
        expectedException.expect(Exception.class);
        expectedException.expectMessage("Titlu invalid!");
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("c");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("edit");
     //   try {
            controller.adaugaCarte(carte);
    //    }catch (Exception ex){
     //       System.out.println(ex.toString());
      //  }
     //   int dimensiuneFisierDupa = cartiRepository.getCarti().size();
//        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC3_ECP() throws Exception{
        //an aparitie < 1000
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        expectedException.expect(Exception.class);
        expectedException.expectMessage("An invalid!");
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("c");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("Titlu");
        carte.setReferenti(referenti);
        carte.setAnAparitie("999");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Ed");
      //  try {
            controller.adaugaCarte(carte);
      //  }catch (Exception ex){
     //       System.out.println(ex.toString());
     //   }
      //  int dimensiuneFisierDupa = cartiRepository.getCarti().size();
//        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }


    @Test
    public void TC4_ECP() {
        //an aparitie > 1000
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("Titlu");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1989");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Ed");
        try {
            controller.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC5_ECP() throws Exception {
        //an aparitie nu e numar
        expectedException.expect(Exception.class);
        expectedException.expectMessage("An invalid!");
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("Titlu");
        carte.setReferenti(referenti);
        carte.setAnAparitie("aaa");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("ed");
    //    try {
            controller.adaugaCarte(carte);
     //   }catch (Exception ex){
     //       System.out.println(ex.toString());
    //    }
        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
//        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC1_BVA() {
        //titlu contine o litera
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("edit");
        try {
            controller.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC2_BVA() {
        //lista de referenti e vida
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
      //  referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("edit");
        try {
            controller.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC4_BVA() {
        //editura contine o litera
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
          referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("E");
        try {
            controller.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    @Test
    public void TC5_BVA() {
        //editura = ""
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
     //   carte.setEditura("E");
        try {
            controller.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }



}