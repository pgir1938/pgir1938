package biblioteca.repository.repoMock;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class Test_Integrare_big_bang {

    CartiRepositoryMock cartiRepository1;
    CartiRepositoryMock cartiRepository2;
    BibliotecaCtrl controller;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception{
        this.cartiRepository1 = new CartiRepositoryMock();
        this.cartiRepository2 = new CartiRepositoryMock();
        this.cartiRepository2.carti.clear();
        this.controller = new BibliotecaCtrl(this.cartiRepository1);
    }

    //functionalitatea 1
    @Test
    public void TC1_BVA() {
        //titlu contine o litera
        int dimensiuneFisierInainte = cartiRepository1.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("T");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("edit");
        try {
            controller.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = cartiRepository1.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    //functionalitatea 2
    @Test
    public void WBT_01(){
        //cautare cu succes
        String name = "Mihai Eminescu";
        int dim_exp = 1;
        int dim_found = cartiRepository1.cautaCarteDupaReferent(name).size();
        assertEquals(dim_exp, dim_found);
    }

    //functionalitatea 3
    @Test
    public void WBT_02(){
        //cautare cu succes
        String an = "1973";
        int dim_exp = 2;
        int dim_found = cartiRepository1.getCartiOrdonateDinAnul(an).size();
        assertEquals(dim_exp, dim_found);
    }
/*               ------ trebuie decomentat----
    @Test
    public void WBT_03(){
        //cautare fara succes
        String an = "197";
        int dim_exp = 0;
        int dim_found = cartiRepository1.getCartiOrdonateDinAnul(an).size();
        assertEquals(dim_exp, dim_found);
    }

    */

    @Test
    public void testare_integrare(){
       // P -> A -> B -> C
        TC1_BVA();
        WBT_01();
        WBT_02();
    }
}
