package biblioteca.repository.repoMock;

import biblioteca.control.BibliotecaCtrl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class CartiRepositoryMockTest {

    BibliotecaCtrl controller;
    //  CartiRepository cartiRepository;
    CartiRepositoryMock cartiRepository1;
    CartiRepositoryMock cartiRepository2;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception{
        this.cartiRepository1 = new CartiRepositoryMock();
        this.cartiRepository2 = new CartiRepositoryMock();
        this.cartiRepository2.carti.clear();
        // this.controller = new BibliotecaCtrl(cartiRepository);
    }


    //lab 3

    @Test
    public void WBT_01(){
        //cautare cu succes
        String name = "Mihai Eminescu";
        int dim_exp = 1;
        int dim_found = cartiRepository1.cautaCarteDupaReferent(name).size();
        assertEquals(dim_exp, dim_found);
    }

    @Test
    public void WBT_02(){
        //cautare fara succes
        String name = "blaaaa";
        int dim_exp = 0;
        int dim_found = cartiRepository1.cautaCarteDupaReferent(name).size();
        assertEquals(dim_exp, dim_found);
    }

   @Test
    public void WBT_03(){
        //cautare in lista de carti care e vida
        String name = "blaaaaaaaaaaaaaaaaaaaaaaa";
        int dim_exp = 0;
        int dim_found = cartiRepository2.cautaCarteDupaReferent(name).size();
        assertEquals(dim_exp, dim_found);
    }

}