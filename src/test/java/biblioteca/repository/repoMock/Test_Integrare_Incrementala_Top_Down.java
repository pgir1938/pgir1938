package biblioteca.repository.repoMock;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class Test_Integrare_Incrementala_Top_Down {

    BibliotecaCtrl controller;
    CartiRepositoryMock cartiRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception{
           this.cartiRepository = new CartiRepositoryMock();
         this.controller = new BibliotecaCtrl(cartiRepository);
    }

    @Test
    public void TC1_ECP() {
        //date valide
        int dimensiuneFisierInainte = cartiRepository.getCarti().size();
        ArrayList<String> referenti = new ArrayList<String>();
        referenti.add("Ref");
        ArrayList<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cu");
        cuvinteCheie.add("c");
        Carte carte = new Carte();
        carte.setTitlu("Titlu");
        carte.setReferenti(referenti);
        carte.setAnAparitie("1889");
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setEditura("Ed");
        try {
            controller.adaugaCarte(carte);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        int dimensiuneFisierDupa = cartiRepository.getCarti().size();
        assertEquals(dimensiuneFisierInainte+1, dimensiuneFisierDupa);
    }

    //integrare AB
    @Test
    public void integrare_B(){
        TC1_ECP();
        //cautare cu succes
        String name = "Mihai Eminescu";
        int dim_exp = 1;
        int dim_found = 0;
        try {
           dim_found = controller.cautaCarte(name).size();
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        assertEquals(dim_exp, dim_found);
    }

    //integrare ABC
    @Test
    public void integrare_C(){
        integrare_B();
        String an = "1973";
        int dim_exp = 2;
        int dim_found = 0;
        try {
           dim_found = controller.getCartiOrdonateDinAnul(an).size();
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
        assertEquals(dim_exp, dim_found);
    }
}
