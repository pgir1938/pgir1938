package biblioteca.control;


import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepository;
import biblioteca.repository.repoInterfaces.CartiRepositoryInterface;
import biblioteca.util.Validator;

import java.util.List;

public class BibliotecaCtrl {

	private CartiRepositoryInterface cr;

	//private CartiRepository cr;
	public BibliotecaCtrl(CartiRepositoryInterface cr){
		this.cr = cr;
	}
	
	public void adaugaCarte(Carte c) throws Exception{
		Validator.validateCarte(c);
		cr.adaugaCarte(c);
	}

	public List<Carte> cautaCarte(String autor) throws Exception{
		Validator.isOKString(autor);
		return cr.cautaCarteDupaReferent(autor);
	}
	
	public List<Carte> getCarti() throws Exception{
		return cr.getCarti();
	}
	
	public List<Carte> getCartiOrdonateDinAnul(String an) throws Exception{
		if(!Validator.isNumber(an))
			throw new Exception("Nu e numar corect!");
		return cr.getCartiOrdonateDinAnul(an);
	}
	
	
}
